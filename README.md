# Personal Portfolio
Built with Bootstrap 4.0 JQuery and HTML5





Credits:
	Original Creator: https://wwww.devcrud.com/
    Demo Images:
        Unsplash:       	(https://www.unsplash.com)

    Icons:
		Themify Icons: 		(https://themify.me/themify-icons)

	Other:
		JQuery: 			(https://www.jquery.com)
		Bootstrap: 			(https://www.getbootstrap.com)
		Bootstrap Affix: 	(http://getbootstrap.com/javascript/#affix)  
		Isotope: 			(https://isotope.metafizzy.co/) 
		Google Maps:		(https://maps.google.com)
